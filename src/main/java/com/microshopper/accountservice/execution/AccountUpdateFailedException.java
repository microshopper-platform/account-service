package com.microshopper.accountservice.execution;

public class AccountUpdateFailedException extends RuntimeException {

  public AccountUpdateFailedException(String message) {
    super(message);
  }
}
