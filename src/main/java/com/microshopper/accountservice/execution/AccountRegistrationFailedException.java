package com.microshopper.accountservice.execution;

public class AccountRegistrationFailedException extends RuntimeException {

  public AccountRegistrationFailedException(String message) {
    super(message);
  }
}
