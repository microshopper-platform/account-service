package com.microshopper.accountservice.mapper;

import com.microshopper.accountservice.model.Account;
import com.microshopper.accountservice.web.dto.AccountDto;
import com.microshopper.accountservice.web.dto.AccountDataDto;
import com.microshopper.accountservice.web.dto.AccountRegistrationDto;
import com.microshopper.authservice.web.dto.AccountRegistration;
import com.microshopper.authservice.web.dto.AccountUpdate;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

  public AccountDto mapToAccountDto(Account account) {

    AccountDto accountDto = new AccountDto();
    accountDto.setId(account.getId());
    accountDto.setUsername(account.getUsername());
    accountDto.setEmail(account.getEmail());
    accountDto.setData(mapToAccountDataDto(account));

    return accountDto;
  }

  public AccountDataDto mapToAccountDataDto(Account account) {

    AccountDataDto accountDataDto = new AccountDataDto();
    accountDataDto.setFirstName(account.getFirstName());
    accountDataDto.setLastName(account.getLastName());
    accountDataDto.setCountry(account.getCountry());
    accountDataDto.setCity(account.getCity());
    accountDataDto.setAddress(account.getAddress());

    return accountDataDto;
  }

  public Account mapFromAccountDataDto(AccountDataDto accountDataDto) {

    Account account = new Account();
    account.setFirstName(accountDataDto.getFirstName());
    account.setLastName(accountDataDto.getLastName());
    account.setAddress(accountDataDto.getAddress());
    account.setCity(accountDataDto.getCity());
    account.setCountry(accountDataDto.getCountry());

    return account;
  }

  public AccountRegistration mapToAccountRegistration(AccountRegistrationDto accountRegistrationDto) {

    AccountRegistration accountRegistration = new AccountRegistration();
    accountRegistration.setUsername(accountRegistrationDto.getUsername());
    accountRegistration.setPassword(accountRegistrationDto.getPassword());
    accountRegistration.setRole(AccountRegistration.RoleEnum.USER);
    accountRegistration.setEmail(accountRegistrationDto.getEmail());

    return accountRegistration;
  }

  public AccountUpdate mapToAccountUpdate(Account account) {

    AccountUpdate accountUpdate = new AccountUpdate();
    accountUpdate.setUsername(account.getUsername());
    accountUpdate.setEmail(account.getEmail());
    accountUpdate.setRole(AccountUpdate.RoleEnum.USER);

    return accountUpdate;
  }
}
