package com.microshopper.accountservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "Account")
public class AccountDto {

  private Long id;

  private String username;

  private String email;

  private AccountDataDto data;
}
