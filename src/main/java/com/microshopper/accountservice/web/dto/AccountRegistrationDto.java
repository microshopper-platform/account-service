package com.microshopper.accountservice.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "AccountRegistration")
public class AccountRegistrationDto {

  @JsonProperty(required = true)
  private String username;

  @JsonProperty(required = true)
  private String password;

  @JsonProperty(required = true)
  private String email;

  @JsonProperty(required = true)
  private AccountDataDto data;
}
