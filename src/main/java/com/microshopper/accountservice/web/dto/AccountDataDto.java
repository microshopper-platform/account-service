package com.microshopper.accountservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "AccountData")
public class AccountDataDto {

  private String firstName;

  private String lastName;

  private String country;

  private String city;

  private String address;
}
