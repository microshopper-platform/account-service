package com.microshopper.accountservice.web.controller;

import com.microshopper.accountservice.service.AccountService;
import com.microshopper.accountservice.web.dto.AccountDto;
import com.microshopper.accountservice.web.dto.AccountDataDto;
import com.microshopper.accountservice.web.dto.AccountRegistrationDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "account", description = "The Account API")
public class AccountController {

  private static final String BASE_URL = "/api/accounts";

  private final AccountService accountService;

  @Autowired
  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @GetMapping(value = BASE_URL + "/{username}")
  @ResponseStatus(value = HttpStatus.OK)
  public AccountDto getAccountByUsername(@PathVariable String username) {

    return accountService.getAccountByUsername(username);
  }

  @GetMapping(value = BASE_URL + "/data/{username}")
  @ResponseStatus(value = HttpStatus.OK)
  public AccountDataDto getAccountDataByUsername(@PathVariable String username) {

    return accountService.getAccountDataByUsername(username);
  }

  @PostMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.CREATED)
  public AccountDto registerAccount(@RequestBody AccountRegistrationDto accountRegistrationDto) {

    return accountService.registerAccount(accountRegistrationDto);
  }

  @PutMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.OK)
  public void updateAccount(@RequestBody AccountDto accountDto) {

    accountService.updateAccount(accountDto);
  }
}
