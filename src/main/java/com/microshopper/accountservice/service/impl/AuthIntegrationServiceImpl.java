package com.microshopper.accountservice.service.impl;

import com.microshopper.accountservice.execution.AccountRegistrationFailedException;
import com.microshopper.accountservice.execution.AccountUpdateFailedException;
import com.microshopper.accountservice.mapper.AccountMapper;
import com.microshopper.accountservice.model.Account;
import com.microshopper.accountservice.service.AuthIntegrationService;
import com.microshopper.accountservice.web.dto.AccountRegistrationDto;
import com.microshopper.authservice.web.api.AuthApi;
import com.microshopper.authservice.web.dto.AccountRegistration;
import com.microshopper.authservice.web.dto.AccountUpdate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthIntegrationServiceImpl implements AuthIntegrationService {

  private final AuthApi authApi;
  private final AccountMapper accountMapper;

  public AuthIntegrationServiceImpl(AuthApi authApi, AccountMapper accountMapper) {
    this.authApi = authApi;
    this.accountMapper = accountMapper;
  }

  @Override
  public void registerAccountToAuthService(Long id, AccountRegistrationDto accountRegistrationDto) {

    AccountRegistration accountRegistration = accountMapper.mapToAccountRegistration(accountRegistrationDto);
    accountRegistration.setId(id);

    ResponseEntity<Void> authRegistrationResponse = authApi.registerAccountWithHttpInfo(accountRegistration);

    if (authRegistrationResponse.getStatusCode().isError()) {
      throw new AccountRegistrationFailedException("Account creation failed. Account registration failed.");
    }
  }

  @Override
  public void updateAccountToAuthService(Account account) {

    AccountUpdate accountUpdate = accountMapper.mapToAccountUpdate(account);

    ResponseEntity<Void> authUpdateResponse = authApi.updateAccountWithHttpInfo(accountUpdate);

    if (authUpdateResponse.getStatusCode().isError()) {
      throw new AccountUpdateFailedException("Account update failed. Account update on auth service failed.");
    }
  }
}
