package com.microshopper.accountservice.service.impl;

import com.microshopper.accountservice.execution.AccountNotFoundException;
import com.microshopper.accountservice.mapper.AccountMapper;
import com.microshopper.accountservice.model.Account;
import com.microshopper.accountservice.repository.AccountRepository;
import com.microshopper.accountservice.service.AccountService;
import com.microshopper.accountservice.service.AuthIntegrationService;
import com.microshopper.accountservice.web.dto.AccountDto;
import com.microshopper.accountservice.web.dto.AccountDataDto;
import com.microshopper.accountservice.web.dto.AccountRegistrationDto;
import com.microshopper.notificationservice.web.api.NotificationApi;
import com.microshopper.notificationservice.web.dto.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

  private final AccountRepository accountRepository;
  private final AccountMapper accountMapper;
  private final AuthIntegrationService authIntegrationService;
  private final NotificationApi notificationApi;

  @Autowired
  public AccountServiceImpl(
    AccountRepository accountRepository,
    AccountMapper accountMapper,
    AuthIntegrationService authIntegrationService,
    NotificationApi notificationApi) {

    this.accountRepository = accountRepository;
    this.accountMapper = accountMapper;
    this.authIntegrationService = authIntegrationService;
    this.notificationApi = notificationApi;
  }

  @Override
  public AccountDto getAccountById(Long id) {

    Account account = accountRepository.getOne(id);

    return accountMapper.mapToAccountDto(account);
  }

  @Override
  public AccountDto getAccountByUsername(String username) {

    Optional<Account> account = accountRepository.getAccountByUsername(username);

    if(account.isEmpty()) {
      throw new AccountNotFoundException("Account with username: " + username + " not found.");
    }

    return accountMapper.mapToAccountDto(account.get());
  }

  @Override
  public AccountDataDto getAccountDataByUsername(String username) {

    Optional<Account> account = accountRepository.getAccountByUsername(username);

    if(account.isEmpty()) {
      throw new AccountNotFoundException("Account with username: " + username + " not found.");
    }

    return accountMapper.mapToAccountDataDto(account.get());
  }

  @Override
  @Transactional
  public AccountDto registerAccount(AccountRegistrationDto accountRegistrationDto) {

    Account account = accountMapper.mapFromAccountDataDto(accountRegistrationDto.getData());

    account.setUsername(accountRegistrationDto.getUsername());
    account.setEmail(accountRegistrationDto.getEmail());

    Account newAccount = accountRepository.save(account);

    authIntegrationService.registerAccountToAuthService(newAccount.getId(), accountRegistrationDto);

    sendNotificationToAccount(accountRegistrationDto.getUsername());

    return accountMapper.mapToAccountDto(newAccount);
  }

  @Override
  @Transactional
  public void updateAccount(AccountDto accountDto) {

    String username = accountDto.getUsername();
    Optional<Account> accountToBeUpdated = accountRepository.getAccountByUsername(username);
    accountToBeUpdated.ifPresentOrElse(account -> {
      account.setEmail(accountDto.getEmail());
      account.setFirstName(accountDto.getData().getFirstName());
      account.setLastName(accountDto.getData().getLastName());
      account.setAddress(accountDto.getData().getAddress());
      account.setCity(accountDto.getData().getCity());
      account.setCountry(accountDto.getData().getCountry());
      Account updatedAccount = accountRepository.save(account);
      authIntegrationService.updateAccountToAuthService(updatedAccount);
    }, () -> { throw new AccountNotFoundException("Account update failed. Account with username: " + username + " not found.");
    });
  }

  private void sendNotificationToAccount(String username) {

    Notification notification = new Notification();
    notification.setTitle("Welcome to Microshopper Platform!");
    notification.setMessage("Thank you for registering!");
    notification.setUsername(username);

    notificationApi.addNotification(notification);
  }
}
