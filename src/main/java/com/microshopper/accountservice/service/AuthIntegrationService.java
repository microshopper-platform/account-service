package com.microshopper.accountservice.service;

import com.microshopper.accountservice.model.Account;
import com.microshopper.accountservice.web.dto.AccountRegistrationDto;

public interface AuthIntegrationService {

  void registerAccountToAuthService(Long id, AccountRegistrationDto accountRegistrationDto);

  void updateAccountToAuthService(Account account);
}
