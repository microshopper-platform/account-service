package com.microshopper.accountservice.service;

import com.microshopper.accountservice.web.dto.AccountDto;
import com.microshopper.accountservice.web.dto.AccountDataDto;
import com.microshopper.accountservice.web.dto.AccountRegistrationDto;

public interface AccountService {

  AccountDto getAccountById(Long id);

  AccountDto getAccountByUsername(String username);

  AccountDataDto getAccountDataByUsername(String username);

  AccountDto registerAccount(AccountRegistrationDto accountRegistrationDto);

  void updateAccount(AccountDto accountDto);
}
