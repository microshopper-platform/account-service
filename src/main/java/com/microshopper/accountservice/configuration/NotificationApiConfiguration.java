package com.microshopper.accountservice.configuration;

import com.microshopper.notificationservice.web.api.NotificationApi;
import com.microshopper.notificationservice.web.restclient.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class NotificationApiConfiguration {

  private final RestTemplate restTemplate;

  @Autowired
  public NotificationApiConfiguration(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Bean
  public ApiClient notificationApiClient() {
    ApiClient apiClient = new ApiClient(restTemplate);
    apiClient.setBasePath("http://notification-service");
    return apiClient;
  }

  @Bean
  public NotificationApi notificationApi() {
    return new NotificationApi(notificationApiClient());
  }
}
