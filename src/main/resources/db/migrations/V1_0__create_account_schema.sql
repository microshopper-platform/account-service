CREATE TABLE account (
    id serial NOT NULL,
    username character varying(32) NOT NULL,
    password character varying(1024) NOT NULL,
    email character varying(128) NOT NULL,
    first_name character varying(64) NOT NULL,
    last_name character varying(64) NOT NULL,
    country character varying(64) NOT NULL,
    city character varying(64) NOT NULL,
    address character varying(128) NOT NULL,
    PRIMARY KEY (id)
);
